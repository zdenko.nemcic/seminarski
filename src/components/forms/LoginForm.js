import React from 'react';


 class LoginForm extends React.Component {
   state = {
       username: '',
       password: ''
   };

   onChangeUsename = (e) => {
    this.setState({username: e.target.value});
   }

   onChangePassword = (e) => {
    this.setState({password: e.target.value});
  }

   onSubmit = (e) => {
     e.preventDefault();
     this.props.onLogin(this.state);  
   };

   render() {
     return(
       <div>
        <form onSubmit = {this.onSubmit} className="login-form">
            <label htmlFor="username"><b>Username</b></label><br/>
            <input className="login-input" type="text" placeholder="Upišite Username" 
            id="username" name="username" value={this.state.username} onChange = {this.onChangeUsename} required/>
            <br/><br/>
            <label htmlFor="password"><b>Password</b></label><br/>
            <input className="login-input" type="password" placeholder="Upišite Password" 
            id="password" name="password" value={this.state.password} onChange = {this.onChangePassword} required/>
            <br/><br/>
            <button className="login-button" type="submit">Login</button>
        </form>
        </div>
     );
    }
}

 export default LoginForm;
