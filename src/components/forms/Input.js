import React from 'react';

export default class Input extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            text: '',
            placeholder: ''
        }
    }

    submitMessage = (e) => {
        e.preventDefault();  
        this.props.onSendMessage(this.state.text);
        this.setState({text: ''});      
    }

    handleText = (e) => {
        this.setState({text: e.target.value, placeholder: e.target.value})
    }

    handleReset = (e) => {
        this.props.onReset();
      };

      catchText = () => {
          this.props.onCatch('Netko tipka ...')
      }

    render(){
        return(
            <div>
                <form onSubmit={this.submitMessage} className="chat-form">
                <button type="button" className="reset" onClick={(e)=>this.handleReset(e)}>Obriši poruke</button>
                <label></label>
                <input className="chat-input" type='text' placeholder='Upišite poruku...' 
                value={this.state.text} onChange={this.handleText} onKeyUp={this.catchText} required/>
                <button className="chat-button" type='submit'value=''>Pošalji</button>
                </form>
            </div>
        );
    }
}