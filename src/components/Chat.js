import React from "react";
import Messages from "./Messages";
import Input from "./forms/Input";

function randomColor() {
  return '#' + Math.floor(Math.random() * 0xFFFFFF).toString(16);
}
class Chat extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      member: {
        color: randomColor(),
        username: this.props.user
      },
      members: []
    };
    
//------------Scaledrone-----------------------------------------------
    //istanca na Scaledrone uspostavlja vezu 
    this.drone = new window.Scaledrone("fpscP72xfAcqm6XE", {
      data: this.state.member //postavljanje podataka o članu u ovom slučaju random (color) i usename peko log forme
    });

    let members = [];
	//Veza je otvorena. Argument error ukazuje na problem s vezom.
    this.drone.on('open', error => {
      if (error) {
        return console.error(error);
      }
	//članu je pored username i color pridodan id 
      const member = {...this.state.member};
      // console.log(member);
      member.id = this.drone.clientId;
      this.setState({member});
    });
    // poruke su raspoređene po sobama. Moramo se prijaviti u željenu sobu sobu.
    const room = this.drone.subscribe("observable-room");
    // Nakon konektiranja u sobu možemo dohvaćati poruke koje su u njoj
    room.on('data', (data, member) => {
      const messages = this.state.messages;
      //dohvaćamo poruke od usera iz sobe te pridodajemo postojećim porukama
      messages.push({member, text: data});
      this.setState({messages});
    });

    room.on('members', m => {
      members = m;
      this.setState({members})
     // updateMembersDOM();
    });
    room.on('member_join', member => {
      members.push(member);
      this.setState({members})
    });
    room.on('member_leave', ({id}) => {
      const index = members.findIndex(member => member.id === id);
      members.splice(index, 1);
      this.setState({members})
    });
//-----------------------------------------------------------------------------

  }
  handleReset=()=>{
    this.setState({messages: []});
  }


  render() {
    const {members} = this.state;

    return (
      <div><p className='online'>Trenutno online: {members.length} članova:&nbsp; 
        {(members) && members.map((m) => {
          return(<span key={m.id}>{m.clientData.username},&nbsp;</span>);
        })}</p>
        
        <Messages
          messages={this.state.messages}
          currentMember={this.state.member}
         
        />
        <Input
          onSendMessage={this.onSendMessage}
          onReset={this.handleReset}
          onCatch={this.catchText}
        />
      </div>
    );
  }

  onSendMessage = (message) => {
    // Slanje poruka u sobu
// Svi korisnici u sobi dobit će poruku
    this.drone.publish({
      room: "observable-room",
      message
    });
  }

  //TODO: prikazati da netko tipka
  catchText = (m) => {
    console.log(m);
  }

}

export default Chat;
