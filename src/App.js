import React from "react";
import "./App.css";
import Chat from "./components/Chat";
import LoginForm from "./components/forms/LoginForm";

class App extends React.Component {

  constructor() {
    super();

    this.state = {
      user: '',
      login: false,
      status: 'Za testiranje aplikacije upišite istu vrijednost za username i passwod!'
    }
  }

  onLogin = (data) => {
    if (data.username === data.password) {
    this.setState({ user: data.username, login: true });
    }else {
      this.setState({ status: 'Neispravan unos, pokušajte ponovo' });
    }
  };
  handleLogout=()=>{
    this.setState({login:false, user: ''});
    window.location.reload(false);
  }

  render() {
    // console.log(this.state.user);
    if(this.state.login){
        return (
        <div className="App">
          <div className="App-header">
        <h1>My Chat App<span className="user">Prijavljeni ste kao:<b> {this.state.user} </b> 
        &nbsp;&nbsp;&nbsp;&nbsp;<span className="odjava" onClick={this.handleLogout}> Odjava</span></span></h1>
        </div>
            <Chat user={this.state.user} onLogout={this.handleLogout}/>
         </div>
      );
        } else {
          return (
            <div className="App">
              <div className="App-header">
            <h1>My Chat App - prijava</h1>
              </div>
          <p id="status">{this.state.status}</p>
                <LoginForm onLogin={this.onLogin} />
            </div>
          );
        }
  }
}

export default App;
